package arrays

// Arrays allow you to store multiple elements of the same type in a variable in a particular order.
// When you have an array, it is very common to have to iterate over them.

func Sum(nums []int) int {
	var result int
	for _, num := range nums {
		result += num
	}
	return result
}

func SumAll(numbersToSum ...[]int) []int {
	var sums []int

	for _, numbers := range numbersToSum {
		sums = append(sums, Sum(numbers))
	}
	
	return sums
}

func SumAllTails(numbersToSum ...[]int) []int {
	var sums []int
	for _, numbers := range numbersToSum {
		if len(numbers) == 0 {
			sums = append(sums, 0)
		} else {
			tail := numbers[1:]
			sums = append(sums, Sum(tail))
		}
	}

	return sums
}