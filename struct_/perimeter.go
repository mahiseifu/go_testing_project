package struct_

// A struct is just a named collection of fields where you can store data.

// A method is a function with a receiver. A method declaration binds an identifier,
// the method name, to a method, and associates the method with the receiver's base type.
// Methods are very similar to functions but they are called by invoking them on an instance of a particular type.
// Where you can just call functions wherever you like, such as Area(rectangle) you can only call methods on "things".

// Interfaces are a very powerful concept in statically typed languages like Go because they allow you to make functions
// that can be used with different types and create highly-decoupled code whilst still maintaining type-safety.

import "math"

type Shape interface {
	Area() float64
	Perimeter() float64
}

type Rectangle struct {
	Width  float64
	Height float64
}

type Circle struct {
	Radius float64
}

type Triangle struct {
	Base float64
	Height float64
}

func (r Rectangle) Perimeter() float64 {
	return (r.Width * 2) + (r.Height * 2)
}

func (r Rectangle) Area() float64 {
	return (r.Width * r.Height)
}

func (c Circle) Area() float64 {
	return math.Pi * c.Radius * c.Radius
}

func (c Circle) Perimeter() float64 {
	return 2 * math.Pi * c.Radius
}

func (t Triangle) Area() float64 {
	return (t.Base * t.Height) * 0.5
}

func (t Triangle) Perimeter() float64 {
	return t.Base * 3
}