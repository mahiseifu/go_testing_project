package mock

// Without mocking important areas of your code will be untested.
// In our case we would not be able to test that our code paused between
// each print but there are countless other examples.

// Calling a service that can fail? Wanting to test your system in a particular state?
// It is very hard to test these scenarios without mocking.

import (
	"fmt"
	"io"
	"time"
)

const finalWord = "Go!"
const countdownStart = 3

type Sleeper interface {
	Sleep()
}

type ConfigurableSleeper struct {
	Duration time.Duration
	SleepTime   func(time.Duration)
}

func (c *ConfigurableSleeper) Sleep() {
	c.SleepTime(c.Duration)
}

func CountDown(out io.Writer, sleeper Sleeper) {
	for i := countdownStart; i > 0; i-- {
		fmt.Fprintf(out, "%d\n", i)
		sleeper.Sleep()
	}
	fmt.Fprint(out, finalWord)
}
