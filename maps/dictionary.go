package maps

// Maps allow you to store items in a manner similar to a dictionary.
// You can think of the key as the word and the value as the definition.

const (
	ErrNotFound   = DictionaryErr("could not find the word you were looking for")
	ErrWordExists = DictionaryErr("cannot add word because it already exists")
	ErrWordDoesNotExist = DictionaryErr("cannot update word because it does not exist")
)

type DictionaryErr string

func (e DictionaryErr) Error() string {
	return string(e)
}

type Dictionary map[string]string

func (d Dictionary) Search(word string) (string, error) {
	result, ok := d[word]
	if !ok {
		return "", ErrNotFound
	}
	return result, nil
}

func (d Dictionary) AddWord(word, definition string) error {
	_, ok := d[word]
	if ok {
		return ErrWordExists
	}
	d[word] = definition
	return nil
}

func (d Dictionary) UpdateWord(word, definition string) error {
	_, ok := d[word]
	if !ok {
		return ErrWordDoesNotExist
	}
	d[word] = definition
	return nil
}

func (d Dictionary) DeleteWord(word string) {
	delete(d, word)
}