package maps

import (
	"testing"
)

func TestSearch(t *testing.T) {
	dictionary := Dictionary{"test": "this is just a test"}

	t.Run("known word", func(t *testing.T) {
		got, _ := dictionary.Search("test")
		want := "this is just a test"

		assertStrings(t, got, want)
	})

	t.Run("unknown word", func(t *testing.T) {
		_, err := dictionary.Search("unknown")

		assertStrings(t, err.Error(), ErrNotFound.Error())
		assertError(t, err, ErrNotFound)
	})
}

func TestAddWord(t *testing.T) {
	t.Run("new word", func(t *testing.T) {
		dictionary := Dictionary{}
		word := "test"
		definition := "this is just a test"

		err := dictionary.AddWord(word, definition)

		assertError(t, err, nil)
		assertDefinition(t, dictionary, word, definition)
	})

	t.Run("existing word", func(t *testing.T) {
		dictionary := Dictionary{"test": "this is just a test"}
		word := "test"
		definition := "this is just a duplicated word test"

		err := dictionary.AddWord(word, definition)

		assertError(t, err, ErrWordExists)
		assertDefinition(t, dictionary, word, "this is just a test")		
	})
}

func TestUpdateWord(t *testing.T) {
	t.Run("update word", func(t *testing.T) {
		dictionary := Dictionary{"test": "this is just a test"}
		word := "test"
		definition := "this is just a updated word test"

		dictionary.UpdateWord(word, definition)

		assertDefinition(t, dictionary, word, definition)		
	})

	t.Run("not existing word", func(t *testing.T) {
		dictionary := Dictionary{}
		word := "test"
		definition := "this is just a update word test"

		err := dictionary.UpdateWord(word, definition)

		assertError(t, err, ErrWordDoesNotExist)
	})	
}

func TestDeleteWord(t *testing.T) {
	t.Run("delete word", func(t *testing.T) {
		dictionary := Dictionary{"test": "this is just a test"}
		word := "test"

		dictionary.DeleteWord(word)
		_, err := dictionary.Search(word)
		if err != ErrNotFound {
			t.Errorf("Expected %q to be deleted", word)
		}
	})
}

func assertDefinition(t testing.TB, dictionary Dictionary, word, definition string) {
	t.Helper()

	got, err := dictionary.Search(word)
	if err != nil {
		t.Fatal("should find added word:", err)
	}
	assertStrings(t, got, definition)
}

func assertStrings(t testing.TB, got, want string) {
	t.Helper()
	if got != want {
		t.Errorf("got %q want %q given, %q", got, want, "test")
	}
}

func assertError(t testing.TB, got, want error) {
	t.Helper()
	if got != want {
		t.Errorf("got error %q want %q", got, want)
	}
}