package main

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestRecordingAndRetrievingThem(t *testing.T) {
	store := NewInMemoryPlayerStore()
	server := PlayerServer{store}
	player := "Pepper"

	server.ServeHTTP(httptest.NewRecorder(), NewPostWinRequest(player))
	server.ServeHTTP(httptest.NewRecorder(), NewPostWinRequest(player))
	server.ServeHTTP(httptest.NewRecorder(), NewPostWinRequest(player))

	response := httptest.NewRecorder()
	server.ServeHTTP(response, NewGetScoreRequest(player))

	AssertStatus(t, response.Code, http.StatusOK)
	AssertResponseBody(t, response.Body.String(), "3")
}
