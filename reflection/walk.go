package reflection

import "reflect"

func walk(x interface{}, fn func(input string)) {
	val := getValue(x)

	numberOfValues := 0
	var getField func(int) reflect.Value
	// declares a variable named getField.
	// This variable is of function type that accepts an int parameter
	// and returns a value of type reflect.Value
	// getField is just declared but not initialized.
	// To use it, you need to assign a function with the same signature to it.

	switch val.Kind() {
	case reflect.String:
		fn(val.String())		
	case reflect.Struct:
		numberOfValues = val.NumField()
		getField  = val.Field
		// Assigning the function val.Field to getField
		// func (v reflect.Value) Field(i int) reflect.Value
		// then you can use getField like a normal function
	case reflect.Slice, reflect.Array:
		numberOfValues = val.Len()
		getField  = val.Index
	case reflect.Map:
		for _, key := range val.MapKeys() {
			walk(val.MapIndex(key).Interface(), fn)
		}
	case reflect.Chan:
		for {
			if v, ok := val.Recv(); ok {
				walk(v.Interface(), fn)
			} else {
				break
			}
		}
	case reflect.Func:
		valFnResult := val.Call(nil)
		for _, res := range valFnResult {
			walk(res.Interface(), fn)
		}
	}

	for i := 0; i < numberOfValues; i++ {
		walk(getField(i).Interface(), fn)
	}
}

func getValue(x interface{}) reflect.Value {
	val := reflect.ValueOf(x)

	if val.Kind() == reflect.Pointer {
		val = val.Elem()
	}
	return val
}