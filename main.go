package main

import (
	"os"
	"time"

	"project/maths"
)

func main() {
	t := time.Now()
	maths.SVGWriter(os.Stdout, t)
}